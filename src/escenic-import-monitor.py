import argparse
from os import listdir
from os.path import isfile, isdir, join
import os
import time
import shutil
import tarfile
import datetime

def uncompress_file(fname, input_dir, output_dir):
    tsprint('Copying ' + join(input_dir, fname) + ' ' + join(output_dir, fname))
    shutil.copy(join(input_dir, fname), join(output_dir, fname))
    tsprint('Uncompressing ' + join(input_dir, fname) + ' into ' + output_dir)
    tar = tarfile.open(join(input_dir, fname))
    tar.extractall(path=output_dir)
    tar.close()
    tsprint('Moving ' + join(input_dir, fname) + ' into processed dir')
    shutil.move(join(input_dir, fname), join(input_dir, 'processed/' + fname))
    tsprint('Done Uncompressing')

def split_batches(output_dir, subpath='xml_cropped', prefix='batch', max_size=10000):
    source_path = join(output_dir, subpath)
    xml_batch = [f for f in listdir(source_path) if isfile(join(source_path, f)) and '.xml' in f]
    batches = [xml_batch[x:x + max_size] for x in range(0, len(xml_batch), max_size)]
    # Move the files to the proper subdirs
    for i in range(len(batches)):
        batch_dir = join(source_path, prefix + str(i))
        os.makedirs(batch_dir, exist_ok=True)
        tsprint('Pouring ' + str(len(batches[i])) + ' files into ' + batch_dir)
        for fname in batches[i]:
            shutil.move(join(source_path, fname), join(batch_dir, fname))
    tsprint('done splitting batch')
    return [join(source_path, d) for d in listdir(source_path)]

def wait_for_escenic_import(batch, escenic_import_dir, frequency=10):
    tsprint('Waiting for Escenic to import batch ' + batch)
    while True:
        nfiles = len([f for f in listdir(escenic_import_dir) if isfile(join(escenic_import_dir, f)) and '.xml' in f])
        # TODO: Write statistics in spreadsheet
        tsprint(str(nfiles) + ' remaining')
        time.sleep(frequency)
        if nfiles == 0:
            break
    return

def tsprint(s):
    print(str(datetime.datetime.now()) + ': ' + s)

if __name__ == "__main__":

    parser = argparse.ArgumentParser('''
        Escenic's import batch process monitor 
        ''')
    parser.add_argument("-i", "--input-directory", dest="input_dir", default="/all-ok-migration", required=False, help="Directory containing tar files", metavar="INPUT_DIR", )
    parser.add_argument("-t", "--tmp-directory", dest="tmp_dir", default="/opt/escenic/syndication/ok/tmp", required=False, help="Escenic import directory", metavar="OUTPUT_DIR", )
    parser.add_argument("-e", "--escenic-import-directory", dest="escenic_import_dir", default="/opt/escenic/syndication/ok/import", required=False, help="Escenic import directory", metavar="OUTPUT_DIR", )
    parser.add_argument("-f", "--frequency", dest="frequency", default="5", type=int, required=False, help="Cron frequency (in seconds)", metavar="FREQUENCY", )
    parser.add_argument("-mbs", "--max-batch-size", dest="max_batch_size", default="10000", type=int, required=False, help="Maximum number of files per batch", metavar="MAX_BATCH_SIZE", )

    args = parser.parse_args()

    input_dir = args.input_dir
    tmp_dir = args.tmp_dir
    escenic_import_dir = args.escenic_import_dir
    frequency = args.frequency
    max_batch_size = args.max_batch_size
    preprocess_dir = join(tmp_dir, 'xml_cropped')

    while True:
        # First, check if there are remaining files to be processed by Escenic
        wait_for_escenic_import('[recovering batch]', escenic_import_dir, frequency)

        # Next, check if there are pending batches. If so, put them in the import directory
        batches = [join(preprocess_dir, f) for f in listdir(preprocess_dir)]
        for batch in batches:
            xml_files = [f for f in listdir(batch) if isfile(join(batch, f)) and '.xml' in f]
            if len(xml_files) == 0:
                tsprint('Eliminating empty batch ' + batch)
                shutil.rmtree(batch, ignore_errors=True)
                continue
            tsprint('Moving files from ' + batch + ' to ' + escenic_import_dir)
            for xml_file in xml_files:
                shutil.move(join(batch, xml_file), join(escenic_import_dir, xml_file))
            shutil.rmtree(batch, ignore_errors=True)
            wait_for_escenic_import(batch, escenic_import_dir, frequency)
        # Get list of tar files in input directory
        fnames = [f for f in listdir(input_dir) if isfile(join(input_dir, f)) and '.tar' in f]
        if len(fnames) == 0:
            tsprint('No tar files to process')
            time.sleep(frequency)
            continue
        fname = fnames[0]
        tsprint(join(input_dir, fname))
        uncompress_file(fname, input_dir, tmp_dir)
        split_batches(tmp_dir, 'xml_cropped', 'batch', max_batch_size)



