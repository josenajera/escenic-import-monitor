import gspread
from oauth2client.service_account import ServiceAccountCredentials
import json
import argparse


def read_google_sheet(spreadsheet_name, sheet_name):
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']

    credentials = ServiceAccountCredentials.from_json_keyfile_name('./gdrive-reach-api-679beffbe53d.json', scope)
    gc = gspread.authorize(credentials)
    worksheet = gc.open(spreadsheet_name).worksheet(sheet_name)
    list_of_lists = worksheet.get_all_values()
    headers = list_of_lists[0]
    return [dict(zip(headers, row)) for row in list_of_lists[1:]]


if __name__ == '__main__':

    # df = read_google_sheet('20190406_Mallorca', 'venues')
    df = read_google_sheet('Dusk: Prod Imports', 'OK')
    print(json.dumps(df,indent=4,ensure_ascii=True))
